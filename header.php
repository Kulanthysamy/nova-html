<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Bootstrap -->
      <link href="assets/css/bootstrap-grid.css" rel="stylesheet">
      <link href="assets/css/bootstrap-grid.rtl.css" rel="stylesheet">
      <link href="assets/css/bootstrap-reboot.css" rel="stylesheet">
      <link href="assets/css/bootstrap-reboot.rtl.css" rel="stylesheet">
      <link href="assets/css/bootstrap-utilities.css" rel="stylesheet">
      <link href="assets/css/bootstrap-utilities.rtl.css" rel="stylesheet">
      <link href="assets/css/bootstrap.css" rel="stylesheet">
      <link href="assets/css/bootstrap.rtl.css" rel="stylesheet">
      <!-- Min css -->
      
     


      <link href="assets/css/main.css" rel="stylesheet">
      <link href="assets/css/style.css" rel="stylesheet">

   
  </head>
  <body>
<nav class="navbar navbar-expand-lg navbar-light bg-white">
  <div class="container">
    <a class="navbar-brand" href="#"><img src="assets/images/logo.png"></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarScroll" aria-controls="navbarScroll" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarScroll">
      <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
       
      </ul>
      <div class="d-flex">
       
        <ul class="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll" style="--bs-scroll-height: 100px;">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#">Login</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Signup</a>
        </li>
        <li class="nav-item">
          <a class="nav-link">English</a>
        </li>
    </ul>
      </div>
    </div>
  </div>
</nav>